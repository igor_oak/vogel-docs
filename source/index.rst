.. Vogel documentation master file, created by
   sphinx-quickstart on Fri Aug  4 17:27:35 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Vogel API Documentação
======================

Este documento descreve como acessar a API do projeto Vogel e dá outras
considerações. Nos exemplos de linha de comando, foi utilizada a
ferramenta `httpie <https://httpie.org/>`__ **apenas como exemplo** -  a API pode ser manipulada a partir de
qualquer ferramenta com interface http, como `postman <https://www.getpostman.com/>`__.


.. toctree::
   :maxdepth: 2
   :caption: Tópicos

   initial_proposal

Índices e Tabelas
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
