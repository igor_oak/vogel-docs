Proposta Inicial
================

Para iniciar o processo de criação de uma nova proposta comercial será necessário enviar os seguintes dados, via
POST para servidor *sthima*. Exemplo (arquivo `data.json`):

::

    {
        "sales_executive": "Nome do executivo de vendas",
        "service_id": "ID do serviço",
        "viability_id": "ID da viabilidade",
        "opportunity_id": "ID da oportunidade",
        "operation_type": "Tipo da Operação",
        "contract_id": "ID do contrato",

        "company_name": "Nome da empresa",
        "company_phone": "51932675478",
        "company_email": "empresa@domain.com",
        "company_cell_phone": "51932674579",
        "company_contact": "Contato na Empresa",
        "cnpj": "47583412000",

        "street": "nome da rua",
        "district": "bairro",
        "city": "cidade",
        "state": "SC",
        "cep": "23456290",
        "latitude": "15.543687",
        "longitude": "-14.234567",

        "trading_name": "Nome fantasia (opcional)",
        "municipal_registration": "Inscrição Municipal (opcional)",
        "state_registration": "Inscrição Estadual (opcional)"
    }

Os últimos 3 campos acima são opcionais. O relacionamento entre o nome dos campos é o seguinte:

::

    Executivo de Vendas: sales_executive
    ID do serviço: service_id
    ID da viabilidade: viability_id
    ID da Oportunidade: opportunity_id
    Tipo da operação: operation_type
    ID do contrato: contract_id

    Nome fantasia: company_name;
    Telefone: company_phone;
    Email: company_email;
    Celular: company_cell_phone;
    Contato na empresa: company_contact;
    CNPJ: cnpj;

    Rua: street;
    Bairro: district;
    Cidade: city;
    Estado: state;
    CEP: cep;
    Latitude: latitude;
    Longitude: longitude;

    Nome fantasia: trading_name;
    Inscrição Municipal: municipal_registration;
    Inscrição Estadual: state_registration;


Inicialização da nova proposta comercial
----------------------------------------

Para inicializar a proposta, o cliente precisa enviar os dados via POST para a URL ``http://hlg.sthima.vogeltelecom.com/api/v1/viabilities/``. Além desses dados, o *cookie homolog_vogel_sso* também deveria ser enviado.
Exemplo na linha de comando (`httpie <http://httpie.org/>`__):

::

    http post http://hlg.sthima.vogeltelecom.com/api/v1/viabilities/ \
    > 'Cookie: homolog_vogel_sso=9laia8e5dkuchrm6f3ji7j8nv2' < data.json # data.json descrito anteriormente
    HTTP/1.0 201 Created
    Allow: GET, POST, HEAD, OPTIONS
    Content-Length: 756
    Content-Type: application/json
    Date: Tue, 08 Aug 2017 18:11:40 GMT
    Server: WSGIServer/0.2 CPython/3.6.1
    Vary: Accept
    X-Frame-Options: SAMEORIGIN

    {
        "cep": "23456290",
        "city": "cidade",
        "cnpj": "47583412000",
        "company_cell_phone": "51932674579",
        "company_contact": "Contato na Empresa",
        "company_email": "empresa@domain.com",
        "company_name": "Nome da empresa",
        "company_phone": "51932675478",
        "contract_id": "ID do contrato",
        "district": "bairro",
        "id": 8,
        "latitude": "15.543687",
        "longitude": "-14.234567",
        "municipal_registration": "Inscrição Municipal (opcional)",
        "operation_type": "Tipo da Operação",
        "opportunity_id": "ID da oportunidade",
        "sales_executive": "Nome do executivo de vendas",
        "service_id": "ID do serviço",
        "state": "SC",
        "state_registration": "Inscrição Estadual (opcional)",
        "street": "nome da rua",
        "trading_name": "Nome fantasia (opcional)",
        "viability_id": "ID da viabilidade",


        "redirect_url": "http://hlg.sthima.vogeltelecom.com/#!/propostas/nova/8/"
    }

Como pode ser visto acima, os campos são retornados na resposta para conferência e um desses campos
(``redirect_url``), representa a URL de redirecionamento para o servidor *sthima* que
conterá esses dados iniciais pré-carregados em um *wizard* para elaboração de uma nova proposta comercial . A seguir,
um outro exemplo, utilizando *JQuery*:

.. code-block:: javascript

    $(function () {
      $("#start_proposal_button").on('click', function () { // um botão qualquer para iniciar o processo.
        // Dados iniciais da proposta.
        var data = {
            "sales_executive": "Nome do executivo de vendas",
            "service_id": "ID do serviço",
            "viability_id": "ID da viabilidade",
            "opportunity_id": "ID da oportunidade",
            "operation_type": "Tipo da Operação",
            "contract_id": "ID do contrato",

            "company_name": "Nome da empresa",
            "company_phone": "51932675478",
            "company_email": "empresa@domain.com",
            "company_cell_phone": "51932674579",
            "company_contact": "Contato na Empresa",
            "cnpj": "47583412000",

            "street": "nome da rua",
            "district": "bairro",
            "city": "cidade",
            "state": "SC",
            "cep": "23456290",
            "latitude": "15.543687",
            "longitude": "-14.234567",

            "trading_name": "Nome fantasia (opcional)",
            "municipal_registration": "Inscrição Municipal (opcional)",
            "state_registration": "Inscrição Estadual (opcional)"
        };

        $.ajax({
          type: "POST",
          url: "http://hlg.sthima.vogeltelecom.com/api/v1/viabilities/",
          contentType: 'application/json; charset=utf-8',
          dataType: "json",
          jsonp: false,
          data: JSON.stringify(data),
          success: function(response) {
            // response.url contém a url de redirecionamento para o wizard no servidor sthima.
            window.location.replace(response.url);
          }
        });
      });
    });
